FROM node:10.9.0
WORKDIR /usr/src/app
COPY . ./
RUN npm install
EXPOSE 3000
CMD ["npm", "start"]