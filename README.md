# Phone Catalogue (FE/BE)

This is a repo containing one of the parts for the GuideSmiths Frontend Challenge. There are 2 repos, one for backend (written in Express.js) and the other one for frontend (written in React.js). The scope of the project is to have a backend that serves some dataset (a phone list with specs) and the corresponding image for each phone and a FE that shows this data. 

I'd like to say I've learnt quite a lot by doing this challenge, since I'm coming from Angular experience. Specially with Redux, now I'm able to understand what's the real use case of it.

## Deployment

    npm start

Both of the projects can be started with just "npm start", when building locally. There are Dockerfiles in each of the repositories so the project can be run also by building (and tagging) and running this.

For convenience, I've uploaded both of the images to public repos in dockerhub, so the easiest and faster way to deploy both projects is just:

    docker-compose up

This way you'll have backend running on localhost:3000 and frontend on localhost:3000

## To improve

There are few things that bother me that could've been better:

- FE:
	- There are some warnings when you build prod, so this can be cleaned up
	- Probably there is a better way (or more than one) to style components, I tried to use makeStyles hook, as written in MaterialUi doc, but couldn't get it working, just moved onto external .css file
	- Overall project structure could've been better
	- There are no tests

- BE:
	- There are no tests
	- Even thought the scope of the project is just a simple rest, I'm aware that by import .json directly in route file (phone.js) this will be a static version of phones.json that won't update if the file is changed locally.