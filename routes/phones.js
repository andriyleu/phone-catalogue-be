var express = require('express');
var router = express.Router();
var phones = require('../mocks/phones.json');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json(phones);
});

module.exports = router;
